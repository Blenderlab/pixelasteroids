# force python 3.* compability
from __future__ import absolute_import, division, print_function
from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
# regular imports below:

import pyglet
pyglet.resource.path = ['./resources']
pyglet.resource.reindex()

background_image = pyglet.resource.image('background.png')

player_image = pyglet.resource.image('orb.png')

laser_images=[
[
    pyglet.resource.image('laser1.png'),
    pyglet.resource.image('laser2.png'),
    pyglet.resource.image('laser3.png'),
    pyglet.resource.image('laser4.png'),
    pyglet.resource.image('laser5.png')
],
[
    pyglet.resource.image('laserg_1.png'),
    pyglet.resource.image('laserg_2.png'),
    pyglet.resource.image('laserg_3.png'),
    pyglet.resource.image('laserg_4.png'),
    pyglet.resource.image('laserg_5.png')
],
[
    pyglet.resource.image('laserr_1.png'),
    pyglet.resource.image('laserr_2.png'),
    pyglet.resource.image('laserr_3.png'),
    pyglet.resource.image('laserr_4.png'),
    pyglet.resource.image('laserr_5.png')
]
]

bullet_image = pyglet.resource.image('shooting3.png')

asteroid_images = [
    pyglet.resource.image('a_object1.png'),
    pyglet.resource.image('a_object2.png'),
    pyglet.resource.image('a_object3.png'),
    pyglet.resource.image('a_object4.png'),
    pyglet.resource.image('a_object5.png'),
    pyglet.resource.image('a_object6.png'),
    pyglet.resource.image('a_object7.png'),
    pyglet.resource.image('a_object8.png'),
    pyglet.resource.image('a_object9.png'),
    ]

def center_image(image):
    """Sets an image's anchor point to its center"""
    image.anchor_x = image.width/2
    image.anchor_y = image.height/2

center_image(player_image)
center_image(bullet_image)
for asteroid_image in asteroid_images:
    center_image(asteroid_image)