# force python 3.* compability
from __future__ import absolute_import, division, print_function
from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
# regular imports below:
from pyglet.gl import *
from game import resources, util
from pyglet.window import key
import math
import random


class PhysicalObject(pyglet.sprite.Sprite):
    def __init__(self, *args, **kwargs):
        super(PhysicalObject, self).__init__(*args, **kwargs)

        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.dead = False
        self.new_objects = []

    def collides_with(self, other_object):
        collision_distance = (self.image.width/2 + other_object.image.width/2)* (self.scale*0.5) 
        actual_distance = util.distance( self.position, other_object.position)
        return (actual_distance <= collision_distance)

    def handle_collision_with(self, other_object):
        #TODO : avoid to kill shot when hitting ships.
        if other_object.__class__ == self.__class__:
            self.dead = False
        else:
            self.dead = True

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt

        self.check_bounds()

    def check_bounds(self):
        min_x = -280
        min_y = -220
        max_x = 280
        max_y = 220
        if self.x < min_x:
            self.x = max_x
        elif self.x > max_x:
            self.x = min_x
        if self.y < min_y:
            self.y = max_y
        elif self.y > max_y:
            self.y = min_y

class Asteroid(PhysicalObject):
    def __init__(self, size, x, y, rot, velocity_x, velocity_y):
        super(Asteroid, self).__init__(img=resources.asteroid_images[random.randint(0,8)])
        self.size = size
        self.x = x
        self.y = y
        self.rot = rot
        self.velocity_x = velocity_x
        self.velocity_y = velocity_y

        self.scale = self.size

    def update(self, dt):
        super(Asteroid, self).update(dt)
        self.rotation += 1.0 / self.size

    def handle_collision_with(self, other_object):
        super(Asteroid, self).handle_collision_with(other_object)
        if self.dead and self.size > 0.25:
            num_asteroids = random.randint(2, 3)
            for i in range(num_asteroids):
                new_asteroid = Asteroid(
                    x=self.x, y=self.y,
                    rot = random.randint(0, 360),
                    velocity_x = random.random() * 30 + self.velocity_x,
                    velocity_y = random.random() * 30 + self.velocity_y,
                    size = self.scale * 0.5,
                )
                self.new_objects.append(new_asteroid)

    def draw(self):
        try:
            super(Asteroid, self).draw()
        except:
            pass
            
class Player(PhysicalObject):

    def __init__(self, *args, **kwargs):
        super(Player, self).__init__(img=resources.player_image, *args, **kwargs)
        self.scale = 0.6
        self.x = 40
        self.y = -100
        self.velocity_x = 0
        self.velocity_y = 0

        self.thrust = 300
        self.rotate_speed = random.random()*50+50
        self.bullet_speed = 180.0
        self.bullet_loaded = True

    def update(self, dt):
        super(Player, self).update(dt)
        if abs(self.rotation)>75:
            self.rotate_speed=-1* self.rotate_speed
        self.rotation += self.rotate_speed * dt
        
     
    def handle_collision_with(self, other_object):
        if isinstance(other_object, Asteroid):
            self.dead = False

    def fire(self):
        if self.bullet_loaded:
            self.bullet_loaded=False
            pyglet.clock.schedule_once(self.reload_bullet, 0.1)
            for i in range(0,5):
                angle_radians = -math.radians(self.rotation-90)
                ship_radius = self.image.width/2
                bullet_x = self.x + math.cos(angle_radians) * ship_radius * self.scale
                bullet_y = self.y + math.sin(angle_radians) * ship_radius* self.scale
                bullet_vx = (
                    self.velocity_x +
                    math.cos(angle_radians) * self.bullet_speed
                )
                bullet_vy = (
                    self.velocity_y +
                    math.sin(angle_radians) * self.bullet_speed
                )
                new_bullet = Bullet(bullet_x, bullet_y, batch=self.batch)
                new_bullet.rotation=self.rotation
                new_bullet.velocity_x = bullet_vx
                new_bullet.velocity_y = bullet_vy

                self.new_objects.append(new_bullet)

    def reload_bullet(self, dt):
        self.bullet_loaded = True


    def draw(self):
        glLoadIdentity()
        super(Player, self).draw()
        qself.engine_sprite.draw()



class Bullet(PhysicalObject):
	"""Bullets fired by the player"""
	def __init__(self, *args, **kwargs):
		#self.laser_animation = pyglet.image.Animation.from_image_sequence(resources.laser_images[random.randint(0,2)], 0.3) 
		
		super(Bullet, self).__init__(resources.laser_images[random.randint(0,2)][random.randint(0,4)], *args, **kwargs)
		self.scale=0.3
		
		pyglet.clock.schedule_once(self.die, 2)
		

	def die(self, dt):
		self.dead = True