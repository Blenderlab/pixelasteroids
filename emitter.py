import dbus
import dbus.service
import dbus.glib

class Emitter(dbus.service.Object):
    """Emitter DBUS service object."""

    def __init__(self, bus_name, object_path):
        """Initialize the emitter DBUS service object."""
        dbus.service.Object.__init__(self, bus_name, object_path)

    @dbus.service.signal('net.multiplayersgame.dbus')
    def fire(self,n):
        """Emmit a test signal."""
        print('Emitted a fire signal:%s'% n)

    @dbus.service.signal('net.multiplayersgame.dbus')
    def quit_signal(self):
        """Emmit a quit signal."""
        print('Emitted a quit signal')