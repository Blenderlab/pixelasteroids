#!/usr/bin/env python
from __future__ import absolute_import, division, print_function
from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
import pyglet
from pyglet import clock, font, image, window
from pyglet.gl import *
from random import uniform
from game import entities, resources
from pyglet.window import key
from game.util import distance
import threading
import time
import dbus
import dbus.service
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop

DBusGMainLoop(set_as_default=True)


window_dimensions = (1024,768)
gamekeys=[key.A,key.Z,key.E,key.R,key.T,key.Y,key.U,key.I,key.O,key.P]


class World(object):

    def __init__(self):
        self.game_objects = []
        self.key_handler = key.KeyStateHandler()
        self.players= []
        for i in range (0,4):
            p=entities.Player()
            p.x=-230+i*120
            p.y=-180
            self.players.append(p)
            self.game_objects.append(p)
        clock.schedule_interval(self.spawn_asteroid, 1)
        clock.schedule_interval(self.update, 1/20)

    def spawn_asteroid(self, dt):
        if len([obj for obj in self.game_objects if isinstance(obj, entities.Asteroid)]) < 20:
            size = uniform(0.8, 1.0)
            x, y =20,uniform(20,40)
            rot = uniform(0.0, 360.0)
            velocity_x = uniform(-30, 30)
            velocity_y = uniform(5, 3)
            ent = entities.Asteroid(size, x, y, rot, velocity_x, velocity_y)
            self.game_objects.append(ent)
            return ent

    def update(self, dt):
        for obj_1 in self.game_objects:
            for obj_2 in self.game_objects:
                if obj_1 is obj_2:
                    continue
                if not obj_1.dead and not obj_2.dead:
                    if obj_1.collides_with(obj_2):
                        obj_1.handle_collision_with(obj_2)
                        obj_2.handle_collision_with(obj_1)

        for obj in self.game_objects:
            obj.update(dt)
            self.game_objects.extend(obj.new_objects)
            obj.new_objects = []

        ids_to_remove = [obj for obj in self.game_objects if obj.dead]
        for id_to_remove in ids_to_remove:
            self.game_objects.remove(id_to_remove)
        i=0
        for gamekey in gamekeys:
            if self.key_handler[gamekey] and self.players[i].bullet_loaded:
                self.players[i].fire()
            i=i+1
        
    def draw(self):
        glClear(GL_COLOR_BUFFER_BIT)
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        try:
            resources.background_image.blit(-400,-300, width=800, height=600)
        except:
            pass

        for ent in self.game_objects:
            try:
                ent.draw()
            except:
                pass


class Camera(object):

    def __init__(self, win, x=0.0, y=0.0, rot=0.0, zoom=1.0):
        self.win = win
        self.x = x
        self.y = y
        self.rot = rot
        self.zoom = zoom

    def worldProjection(self):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        widthRatio = self.win.width / self.win.height
        gluOrtho2D(
            -self.zoom * widthRatio,
            self.zoom * widthRatio,
            -self.zoom,
            self.zoom)

    def hudProjection(self):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluOrtho2D(0, self.win.width, 0, self.win.height)


class Hud(object):
    def __init__(self, win):
        self.fps = clock.ClockDisplay()

    def draw(self):
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        #self.fps.draw()

class App(object):
    def __init__(self):
        self.world = World()
        self.win = window.Window(fullscreen=False, vsync=True, width=window_dimensions[0], height=window_dimensions[1])
        self.camera = Camera(self.win, zoom=200.0)
        self.hud = Hud(self.win)
        self.win.push_handlers(self.world.key_handler)
        self.dbus_th= DbusThread(name = "DbusGame-{}".format( 1)) # ...Instancie un thread et lui donne un ID unique
        self.dbus_th.start()            
        clock.set_fps_limit(60)

    def mainLoop(self):
        while not self.win.has_exit:
            self.win.dispatch_events()
            self.camera.worldProjection()
            self.world.draw()
            self.camera.hudProjection()
            self.hud.draw()
            clock.tick()
            self.win.flip()

            #if self.world.player.dead is True:
            #   self.world = World last):
            #  self.win.push_handlers(self.world.player.key_handler)

def fire_handler(n):
    """Signal handler for quitting the receiver."""
    try:
        print('RECEIVED fire for %s' % n)
        app.world.players[n].fire()
    except:
        pass

def catchall_signal_handler(*args, **kwargs):
    print ("Caught signal (in catchall handler) "
           + kwargs['dbus_interface'] + "." + kwargs['member'])
    for arg in args:
        print ("    " + str(arg))


class DbusThread(threading.Thread):
        
        def __init__(self,name):
            threading.Thread.__init__(self)

        
        def run(self):
            print("{} started!".format(self.getName()))          
            bus = dbus.SessionBus()
            bus_name = dbus.service.BusName('net.multiplayersgame.dbus', bus=bus)
            #bus.add_signal_receiver(catchall_signal_handler, interface_keyword='dbus_interface', member_keyword='member')
            bus.add_signal_receiver(fire_handler,
                        dbus_interface='net.multiplayersgame.dbus',
                        signal_name='fire')

            loop = GLib.MainLoop()
            loop.run()


app = App()
app.mainLoop()
